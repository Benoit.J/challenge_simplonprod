# Challenge Simplon Prod

###  Summary :

1. [Presentation of the challenge](#presentation of the challenge)
2. [Technology used](#technology used)
3. [Interest](#interest)

### Presentation of the challenge

**Mini API Rest en Laravel**
 Challenge Simplon Prod for application professionalization contract

​               Technical test for Simplon Prod
​               This is a mini API Rest in Laravel with a first entity Promo in relation to Many to One                         

​               and a second entity Learning in relation One to One.

* Time: 1 week

##  Technology used

- Laravel 5.1

##  Interests 


Getting started and learning the Laravel framework.
I liked the handling of the documentation.

  Strong points

- Beginning with the interesting Laravel framework


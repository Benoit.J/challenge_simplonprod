<?php

use Illuminate\Database\Seeder;

class PromoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promos')->insert([
            'title' => 'Promo 1',
            'start_date' => date('2001-12-02'),
            'end_date' => date('2002-06-05'),
        ]);

        DB::table('promos')->insert([
            'title' => 'Promo 2',
            'start_date' => date('2002-12-02'),
            'end_date' => date('2003-06-05'),
        ]);

        DB::table('promos')->insert([
            'title' => 'Promo 3',
            'start_date' => date('2006-12-02'),
            'end_date' => date('2008-06-05'),
        ]);
    }
}

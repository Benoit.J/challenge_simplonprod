<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'name' => ' Galvan',
            'surname' => 'Melanie',
            'email' => 'galvanmel@gmail.com',
            'external_url' => 'http://mel.com',
            'promo_id' => 1
        ]);

        DB::table('students')->insert([
            'name' => ' Isufri',
            'surname' => 'Liri',
            'email' => 'liri@gmail.com',
            'external_url' => 'http://liri.com',
            'promo_id' => 2
        ]);

        DB::table('students')->insert([
            'name' => ' Granet',
            'surname' => 'Jean-Jacques',
            'email' => 'jeanjack@gmail.com',
            'external_url' => 'http://jeanjack.com',
            'promo_id' => 2
        ]);

        DB::table('students')->insert([
            'name' => ' Aertega',
            'surname' => 'Louis',
            'email' => 'louisa@gmail.com',
            'external_url' => 'http://louis.com',
            'promo_id' => 1
        ]);

        DB::table('students')->insert([
            'name' => ' Sebasteneli',
            'surname' => 'Thomas',
            'email' => 'thomass@gmail.com',
            'external_url' => 'http://thomas.com',
            'promo_id' => 3
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromoController extends Controller
{
    public function index()
    {
        return response(Promo::with('students')->get(), 200)
                ->header('Content-Type', 'application/json');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    use Notifiable;

    protected $fillable = [
        "title", "start_date", "end_date"
    ];

    public function students ()
    {
        return $this->hasMany(Student::class);
    }
}

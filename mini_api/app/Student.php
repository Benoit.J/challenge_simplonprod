<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Student extends Model
{
    use Notifiable;

    protected $fillable = [
            "name", "surname", "email", "external_url"
    ];

    public function promos()
    {
        return $this->belongsTo(Promo::class);
    }
}
